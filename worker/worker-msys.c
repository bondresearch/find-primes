#ifdef __MSYS__

#include <w32api/windows.h>
#include <w32api/wincon.h>

extern void sig_handler(int signo);

BOOL ConsoleCtrlHandler(DWORD /* CtrlType */)
{
    sig_handler(0);
    return FALSE;
}

void setConsoleCtrlHandler()
{
    SetConsoleCtrlHandler(ConsoleCtrlHandler, TRUE);
}

#endif
