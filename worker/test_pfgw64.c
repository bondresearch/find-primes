#include <stdio.h>

#include <pfgw/primeformpch.h>
#include <pflib/pfstring.h>
#include <pfgw/pfgw_version.h>
#include <pfgw/pfgw_globals.h>

PrimeServer *primeserver;

int pfgw_init()
{
   getCpuInfo();
   crc_init();

   gwinit2(&gwdata, sizeof(gwhandle), (char *) GWNUM_VERSION);

   if (gwdata.GWERROR == GWERROR_VERSION_MISMATCH)
   {
      printf("GWNUM version mismatch.  PFGW is not linked with version %s of GWNUM.\n", GWNUM_VERSION);
      return -1; 
   }

   if (gwdata.GWERROR == GWERROR_STRUCT_SIZE_MISMATCH)
   {
      printf("GWNUM struct size mismatch.  PFGW must be compiled with same switches as GWNUM.\n");
      return -1;
   }

   char Buffer[512];
   getCpuDescription(Buffer, 1);
   printf("%s\n", Buffer);

   // Create a console output object.
   pOutputObj = new PFConsoleOutput;
   return 0;
}

int main(int argc, const char *argv[])
{
    if (argc < 2) {
        printf("USAGE:\n\t%s number\n", argv[0]);
        return -1;
    }
    pfgw_init();
    Integer n;
    uint64 residual;
    n.atoI(argv[1]);
    int ret = gwPRP(&n, argv[1], &residual);
    printf("%s is %s\n", argv[1], ret == 0 ? "composite" : "PRP-3");
    return 0;
}
