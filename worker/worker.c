#include "../common/work.h"
#include "../common/gmp/gmp_main.h"
#include "../common/tcp.h"

#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <stdlib.h>
#include <gmp.h>
#include <sys/types.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <pfgw/primeformpch.h>
#include <pflib/pfstring.h>
#include <pfio/pfini.h>
#include <pfgw/pfgw_version.h>
#include <pfgw/pfgw_globals.h>

extern void setConsoleCtrlHandler();

#define SERVER_ADDR "85.204.7.150"
//#define SERVER_ADDR "127.0.0.1"

static FILE *log_file;
static const int sleep_time = 5;
static int pending_work;

int test_mode;

int silent = 0;
FILE *get_log_file()
{
    char filename[1024];
    memset(filename, 0, sizeof(filename));
    snprintf(filename, sizeof(filename), "worker_%d.log", getpid());
    if (log_file == NULL)
        log_file = fopen(filename, "w");
    return log_file;
}

int reschedule_work(int work)
{
    ssize_t count = 0;
    int cmd = 0;
    while (1) {
        LOG("Connecting to server for rescheduling work\n");
        int fd = connect_to_tcp_server(SERVER_ADDR, 8080);
        if (fd < 0) {
            LOG("Failed to connect to server errno=%d\n", errno);
            goto retry;
        }
        cmd = (work<< 2) | REQUEST_WORK;
        LOG("Rescheduling work %d\n", work);
        count = send(fd, &cmd, sizeof(cmd), 0);
        if (count != sizeof(cmd)) {
            LOG("Failed while sending job result count=%zu errno=%d\n", count, errno);
            close(fd);
            goto retry;
        }
        close(fd);
        LOG("Rescheduled work successfully\n");
        break;
retry:
        LOG("Failed to reschedule work for %d. Retrying in %d seconds\n", work, sleep_time);
        sleep(sleep_time);
    }
    pending_work = 0;
    return 0;
}

int report_result(int isprime, int work)
{
    LOG("Connecting to server for reporting result\n");
    int fd = connect_to_tcp_server(SERVER_ADDR, 8080);
    if (fd < 0) {
        LOG("Failed to connect to server errno=%d\n", errno);
        return -1;
    }
    int cmd = (work<< 2) | (isprime ? WORK_DONE_PRIME : WORK_DONE_NOT_PRIME);
    LOG("Reporting result for work %d\n", work);
    ssize_t count = send(fd, &cmd, sizeof(cmd), 0);
    if (count != sizeof(cmd)) {
        LOG("Failed while sending job result count=%zu errno=%d\n", count, errno);
        close(fd);
        return -1;
    }
    LOG("Reported work successfully\n");
    pending_work = 0;
    close(fd);
    return 0;
}

static const int MaxCallbacks = 10;
static int num_callbacks;
static int (*callbacks[MaxCallbacks])(char *);

int do_work(const unsigned char *pi_digits, int work)
{
    char number[1024000];
    int j;

    memset(number, 0, sizeof(number));
    number[0] = '3';
    for (j = 0; j < work; ++j)
        number[j+1] = pi_digits[j] + '0';
    number[work+1] = 0;

    int isprime = -1;
    for (j = 0; j < num_callbacks; ++j) {
        isprime = callbacks[j](number);
        if (!isprime)
            break;
    }
    if (isprime < 0)
        return -1;

    LOG("%d is %sprime\n", work, isprime ? "" : "not ");
    if (!test_mode) {
        while (1) {
            int ret = report_result(isprime, work);
            if (ret == 0)
                break;

            LOG("Failed to report result for %d. Retrying in %d seconds\n", work, sleep_time);
            sleep(sleep_time);
        }
    }
    return 0;
}

int get_work(int fd)
{
    int cmd = REQUEST_WORK;
    ssize_t count = send(fd, &cmd, sizeof(cmd), 0);
    if (count != sizeof(cmd)) {
        LOG("Failed while request job count=%zu errno=%d\n", count, errno);
        return -1;
    }
    int work = 0;

    while (work == 0) {
        count = recv(fd, &work, sizeof(work), 0);
        if (count != sizeof(work)) {
            LOG("Failed while receiving job count=%zu errno=%d\n", count, errno);
            return -1;
        }
    }
    pending_work = work;
    return work;
}

void sig_handler(int signo) {
    LOG("Received Signal %d\n", signo);
    if (pending_work)
        reschedule_work(pending_work);
#ifndef __MSYS__
    exit(1);
#endif
}

PrimeServer *primeserver;

int pfgw_init()
{
   getCpuInfo();
   crc_init();

   gwinit2(&gwdata, sizeof(gwhandle), (char *) GWNUM_VERSION);

   if (gwdata.GWERROR == GWERROR_VERSION_MISMATCH)
   {
      printf("GWNUM version mismatch.  PFGW is not linked with version %s of GWNUM.\n", GWNUM_VERSION);
      return -1; 
   }

   if (gwdata.GWERROR == GWERROR_STRUCT_SIZE_MISMATCH)
   {
      printf("GWNUM struct size mismatch.  PFGW must be compiled with same switches as GWNUM.\n");
      return -1;
   }

   char Buffer[512];
   getCpuDescription(Buffer, 1);
   printf("%s\n", Buffer);

   // Create a console output object.
   pOutputObj = new PFConsoleOutput;
   char iname[1024];
   snprintf(iname, sizeof(iname), "pfgw_%d.ini", getpid());
   g_pIni = new PFIni(iname);

   return 0;
}

int check_prime_pos(char *number)
{
    mpz_t n;
    mpz_init(n);
    if (mpz_set_str(n, number, 10)) {
        LOG("Failed to convert number to mpz_t\n");
        return -1;
    }
    LOG("Checking if number is a possible prime.\n");
    int isprime = _GMP_is_pos_prime(n);;
    if (isprime)
        LOG("The number is a possible prime. Continuing with the next test.\n");
    else
        LOG("The number is composite. Stopping here.\n");

    mpz_clear(n);
    return isprime;
}

int check_pfgw_prime(char *number)
{
    Integer n;
    uint64 residual;
    n.atoI(number);
    LOG("Checking if the number is prime (PFGW).\n");
    int isprime = gwPRP(&n, number, &residual);
    if (isprime < 0) {
        LOG("PFGW failed\n");
        return -1;
    }
    if (isprime)
        LOG("PFGW reported the number as prime. Continuing with the next test.\n");
    else
        LOG("The number is composite. Stopping here.\n");

    return isprime;
}

int check_gmp_prime(char *number)
{
    mpz_t n;
    mpz_init(n);
    if (mpz_set_str(n, number, 10)) {
        LOG("Failed to convert number to mpz_t\n");
        return -1;
    }
    LOG("Checking if the number is prime (GMP).\n");
    int isprime = _GMP_is_prime(n);
    if (isprime)
        LOG("The number is prime. Stopping here.\n");
    else
        LOG("The number is composite. Stopping here.\n");

    mpz_clear(n);
    return isprime;
}

int main(int argc, const char *argv[])
{
    char line [1024];
    char *ret;
    unsigned char *pi_digits;
    int pi_count = 0;
    int fd;

    if (argc > 1 && strcmp(argv[1], "test") == 0) {
        if (argc != 3) {
            LOG("USAGE: %s [test number]\n", argv[0]);
            return -1;
        }
        test_mode = 1;
        LOG("Starting in test mode. Checking if first %s digits form a prime.\n", argv[2]);
    } else if (argc > 1 && strcmp(argv[1], "reschedule") == 0) {
        if (argc != 3) {
            LOG("USAGE: %s [reschedule number]\n", argv[0]);
            return -1;
        }
        LOG("Starting in rescheduling mode.\n");
        reschedule_work(atoi(argv[2]));
        return 0;
    }

#ifndef __MSYS__
    struct sigaction act;
    act.sa_handler = &sig_handler;
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    sigaction(SIGHUP, &act, NULL);
#else
    setConsoleCtrlHandler();
#endif

    pi_digits = (unsigned char *)malloc(200000000);

    int i;
    FILE *fin = fopen("../pi.txt", "r");
    if (fin == NULL) {
        LOG("Failed to open pi.txt\n");
        return -1;
    }
    while (pi_count < 1000000) {
        ret = fgets(line, sizeof(line), fin);
        if (ret == NULL)
            break;

        int len = strlen(line);
        for (i = 0; i < len; ++i)
            if (line[i] >= '0' && line[i] <= '9')
                pi_digits[pi_count++] = line[i] - '0';
    } 
    fclose(fin);
    LOG("Read %u digits of PI\n", pi_count);

    _GMP_init();
    callbacks[num_callbacks++] = check_prime_pos;
    if (pfgw_init() < 0)
        LOG("Failed to init pfgw. Will not use it for testing.\n");
    else
        callbacks[num_callbacks++] = check_pfgw_prime;
    callbacks[num_callbacks++] = check_gmp_prime;

    if (test_mode) {
        int work = atoi(argv[2]);
        if (do_work(pi_digits, work))
            LOG("Failed to do work.\n");
        _GMP_destroy();
        return 0;
    }
    while (1) {
        LOG("Connecting to server for requesting work\n");
        fd = connect_to_tcp_server(SERVER_ADDR, 8080);
        if (fd < 0) {
            LOG("Failed while connecting to server\n");
            goto retry;
        }
        LOG("Connected to server\n");
        int work;
        work = get_work(fd);
        LOG("Got work %d\n", work);
        if (work <= 0) {
            LOG("Failed to get work from server\n");
            close(fd);
            goto retry;
        }
        close(fd);
        if (do_work(pi_digits, work)) {
            LOG("Failed to do work. Rescheduling...\n");
            reschedule_work(work);
            goto retry;
        }
        continue;
retry:
        LOG("Retrying after %d seconds\n", sleep_time);
        sleep(sleep_time);
    }
    _GMP_destroy();
}
