#include "../common/tcp.h"
#include "../common/work.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdint.h>

#define MAX_EVENTS 1024
#define MAX_DIGITS 1000000
#define MAX_WORKERS 10240
#define MIN_WORKS 100

static pthread_t epoll_thread;
static struct epoll_event epoll_events[MAX_EVENTS];
static int server_fd;
static int epoll_fd;

static int tosend[MAX_WORKERS];

struct pending_work {
    int work;
    struct timeval tv;
};
static struct pending_work pending_works[MAX_WORKERS];
static int pending_count;
static int future_works[MAX_WORKERS];
static int future_count;
static int reports[MAX_WORKERS];
static int reports_count;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static FILE *primef;
static FILE *non_primef;
static FILE *log_file;
static int primes[MAX_DIGITS];
static int primes_count;
static int non_primes[MAX_DIGITS];
static int non_primes_count;
static int max_verified;

FILE *get_log_file()
{
    if (log_file == NULL)
        log_file = fopen("server.log", "w");
    return log_file;
}

void lock()
{
    pthread_mutex_lock(&mutex);
}

void unlock()
{
    pthread_mutex_unlock(&mutex);
}

int remove_generic_work(int work, int *works, int *work_count)
{
    int i, j;
    lock();
    for (i = 0; i < *work_count; ++i)
        if (works[i] == work)
            break;
    //did not find work
    if (i == *work_count) {
        unlock();
        return -1;
    }
    for (j = i; j < *work_count - 1; ++j)
        works[j] = works[j + 1];
    --(*work_count);
    unlock();
    return 0;
}

int dump_pending_works()
{
    FILE *f = fopen("pending.log", "w");
    if (f == NULL) {
        LOG("Failed to open the pending works file\n");
        return -1;
    }
    struct timeval tv;
    int i;
    gettimeofday(&tv, NULL);
    for (i = 0; i < pending_count; ++i) {
        long long elapsed = ((tv.tv_sec-pending_works[i].tv.tv_sec)*1000000LL + tv.tv_usec-pending_works[i].tv.tv_usec)/1000;
        fprintf(f, "Work: %d is %.2lld:%.2lld:%.2lld:%.3lld old\n", pending_works[i].work, elapsed / 3600000, (elapsed % 3600000) / 60000, (elapsed % 60000) / 1000, elapsed % 1000);
    }
    fclose(f);
    return 0;
}

int remove_pending_work(int work)
{
    int i, j;
    lock();
    for (i = 0; i < pending_count; ++i)
        if (pending_works[i].work == work)
            break;
    //did not find work
    if (i == pending_count) {
        unlock();
        return -1;
    }
    for (j = i; j < pending_count - 1; ++j)
        memcpy(&pending_works[j], &pending_works[j + 1], sizeof(pending_works[j]));
    --pending_count;
    unlock();
    return 0;
}

int remove_future_work(int work)
{
    return remove_generic_work(work, future_works, &future_count);
}

int get_next_available_work()
{
    int i, work;
    lock();
    if (future_count == 0) {
        unlock();
        return -1;
    }
    work = future_works[0];
    for (i = 0; i < future_count - 1; ++i)
        future_works[i] = future_works[i + 1];
    --future_count;
    int pos = pending_count++;
    pending_works[pos].work = work;
    gettimeofday(&pending_works[pos].tv, NULL);
    unlock();
    return work;
}

int add_future_work(int work)
{
    lock();
    future_works[future_count++] = work;
    LOG("Added work %d\n", work);
    unlock();
    return 0;
}

int error_on_worker(int fd)
{
    close(fd);
    return 0;
}

void remove_work(int work)
{
    if (remove_pending_work(work) == 0)
        return;
    LOG("Searching for work %d in future_works to remove it.\n", work);
    if (remove_future_work(work) == 0)
        return;
    LOG("Did not find work %d in future_works. Added in the reports queue\n", work);
    //the reported work is not present in any queue
    lock();
    reports[reports_count++] = work;
    unlock();
}

int handle_request(int fd, int cmd)
{
    int work = cmd >> 2;
    cmd &= 0x3;
    LOG("Connection %d received request %d\n", fd, cmd);
    if (cmd == WORK_DONE_PRIME) {
        if (work == 0) {
            LOG("Ignoring old version worker\n");
            close(fd);
            return 0;
        }
        LOG("Found prime having %d digits by fd %d\n", work, fd);
        fprintf(primef, "%d\n", work);
        fflush(primef);
        remove_work(work);
        close(fd);
        return 0;
    } else if (cmd == WORK_DONE_NOT_PRIME) {
        if (work == 0) {
            LOG("Ignoring old version worker\n");
            close(fd);
            return 0;
        }
        LOG("%d digits is not prime\n", work);
        fprintf(non_primef, "%d\n", work);
        fflush(non_primef);
        remove_work(work);
        close(fd);
        return 0;
    }

    if (cmd != REQUEST_WORK) {
        LOG("Unknown command %d\n", cmd);
        close(fd);
        return -1;
    }
    
    if (work != 0) {
        if (remove_pending_work(work) == 0)
            add_future_work(work);
        close(fd);
        return 0;
    }

    work = get_next_available_work();
    if (work <= 0) {
        LOG("No more work available\n");
        close(fd);
        return -1;
    }

    ssize_t count;
    LOG("Sending work %d to fd %d\n", work, fd);
    count = send(fd, &work, sizeof(work), 0);
    if (count == -1) {
        if (errno != EAGAIN) {
            LOG("Failed while trying to send on connection %d: %d\n", fd, errno);
            close(fd);
            return -1;
        }
        tosend[fd] = work;
        LOG("EAGAIN\n");
    }
    close(fd);
    return 0;
}

int receive_data(int fd)
{
    while (1) {
        ssize_t count;
        int cmd;

        count = read(fd, &cmd, sizeof(cmd));
        if (count == -1) {
            if (errno != EAGAIN) {
                perror ("read");
                error_on_worker(fd);
                return -1;
            }
            return 0;
        }
        else if (count == 0) {
            error_on_worker(fd);
            return -1;
        }
        else if (count != sizeof(cmd)) {
            LOG("Received incomplete command of size %zu\n", count);
            error_on_worker(fd);
            return -1;
        }
        //someone is trying to access this using HTTP GET
        if (cmd == 0x20544547) {
            LOG("This IP should be blacklisted.\n");
            error_on_worker(fd);
            return -1;
        } 
        if (handle_request(fd, cmd) < 0) {
            error_on_worker(fd);
            return -1;
        }
        break;
    }
    return 0;
}

static void *epoll_thread_callback(void *i_arg)
{
    ((void)(i_arg));
    LOG("Started server thread\n");
    while (1) {
        int n, i;

        n = epoll_wait (epoll_fd, epoll_events, MAX_EVENTS, -1);
        for (i = 0; i < n; i++) {
            int fd = epoll_events[i].data.fd;
            if ((epoll_events[i].events & EPOLLERR) || (epoll_events[i].events & EPOLLHUP) || (!(epoll_events[i].events & EPOLLIN) && !(epoll_events[i].events & EPOLLOUT))) {
                LOG("epoll error on fd %d\n", fd);
                if (fd != server_fd)
                    error_on_worker(fd);
                else
                    close(fd);
                continue;
            } else if (server_fd == fd) {
                accept_new_connections(server_fd, epoll_fd);
                continue;
            } else if (epoll_events[i].events & EPOLLIN ) {
                receive_data(fd);
            } else if (epoll_events[i].events & EPOLLOUT) {
                ssize_t count;
                int work = tosend[fd];
                if (work <= 0)
                    continue;
                count = send(fd, &work, sizeof(work), 0);
                if (count == -1) {
                    if (errno != EAGAIN) {
                        LOG("Failed while trying to send on connection %d: %d\n", fd, errno);
                        error_on_worker(fd);
                        continue;
                    }
                }
                tosend[fd] = 0;
                close(fd);
            }
        }
    }

    return 0;
}

int start_epoll_thread()
{
    int ret;
    pthread_attr_t attr;
    ret = pthread_attr_init(&attr);
    if (ret < 0) {
        LOG("failed to init pthread attributes: %d\n", ret);
        return -1;
    }
    ret = pthread_create(&epoll_thread, &attr, &epoll_thread_callback, NULL);
    if (ret < 0) {
        LOG("failed to create tcp server thread: %d\n", ret);
        return -1;
    }
    return 0;
}

int verified_already(int n)
{
    int i;
    lock();
    for (i = 0; i < reports_count; ++i)
        if (reports[i] == n) {
            unlock();
            return 1;
        }
    unlock();
    if (n > max_verified)
        return 0;
    for (i = 0; i < primes_count; ++i)
        if (primes[i] == n)
            return 1;
    for (i = 0; i < non_primes_count; ++i)
        if (non_primes[i] == n)
            return 1;
    return 0;
}

int get_next_possible_prime(int *i, unsigned *sum, const unsigned char *pi_digits, int pi_count)
{
    while (*i < pi_count) {
        unsigned char digit = pi_digits[(*i)++];
        *sum += digit;
        if (*sum % 3 == 0)
            continue;
        if (digit % 2 == 0)
            continue;
        if (digit == 5)
            continue;
        if (!verified_already(*i))
            return *i;
    }
    return 0;
}

int init_files()
{
    char *ret;
    char line[1024];
    primef = fopen("primes.txt", "r");
    if (primef != NULL) {
        ret = fgets(line, sizeof(line), primef);
        while (ret) {
            int work = atoi(line);
            primes[primes_count++] = work;
            if (work > max_verified)
                max_verified = work;
            ret = fgets(line, sizeof(line), primef);
        }
        fclose(primef);
    }
    primef = fopen("primes.txt", "a+");
    if (primef == NULL) {
        LOG("failed to open primes.txt file\n");
        return -1;
    }
    non_primef = fopen("nonprimes.txt", "r");
    if (non_primef != NULL) {
        ret = fgets(line, sizeof(line), non_primef);
        while (ret) {
            int work = atoi(line);
            non_primes[non_primes_count++] = work;
            if (work > max_verified)
                max_verified = work;
            ret = fgets(line, sizeof(line), non_primef);
        }
        fclose(non_primef);
    }
    non_primef = fopen("nonprimes.txt", "a+");
    if (non_primef == NULL) {
        LOG("failed to open non_primes.txt file\n");
        return -1;
    }
    return 0;
}

int close_files()
{
    fclose(primef);
    fclose(non_primef);
    return 0;
}

int silent = 0;

int main(int argc, const char *argv[])
{
    char line [1024];
    char *ret;
    unsigned char *pi_digits;
    int pi_count = 0;
    if (argc < 2) {
        LOG("USAGE:\n\t%s start_digits [silent]\n", argv[0]);
        return -1;
    }

    if (argc == 3)
        silent = !strcmp(argv[2], "silent");

    pi_digits = malloc(MAX_DIGITS);

    int i;
    FILE *fin = fopen("../pi.txt", "r");
    if (fin == NULL) {
        fprintf(stderr, "Failed to open pi.txt\n");
        return -1;
    }
    while (pi_count < MAX_DIGITS) {
        ret = fgets(line, sizeof(line), fin);
        if (ret == NULL)
            break;

        int len = strlen(line);
        for (i = 0; i < len; ++i)
            if (line[i] >= '0' && line[i] <= '9')
                pi_digits[pi_count++] = line[i] - '0';
    } 
    fclose(fin);
    if (init_files() < 0) {
        LOG("failed to init files\n"); 
        return -1;
    }
    LOG("Read %u digits of PI\n", pi_count);
    int start_digits;
    start_digits = atoi(argv[1]);
    server_fd = start_tcp_server(8080, &epoll_fd);
    if (server_fd < 0) {
        LOG("Failed to start server\n");
        return -1;
    }
    if (start_epoll_thread(server_fd) < 0)
        return -1;

    LOG("Started server, start_digits=%d\n", start_digits);

    unsigned sum = 3;
    for (i = 0; i < start_digits && i < pi_count; ++i)
        sum += pi_digits[i];

    i = start_digits; 

    int old_count = 0;
    int round = 0;
    while ((i < MAX_DIGITS && i < pi_count) || future_count || pending_count) {
        int show = 0;
        lock();
        if (old_count != pending_count) {
            old_count = pending_count;
            show = 1;
        }
        if (round % 120 == 0 || show)
            dump_pending_works();
        if (round % 240 == 0 || show)
            LOG("We have %d pending works. If nobody killed a worker, we have that many workers.\n", pending_count);
        while (future_count < MIN_WORKS) {
            unlock();
            int work;
            work = get_next_possible_prime(&i, &sum, pi_digits, pi_count);
            if (work <= 0) {
                LOG("No more primes\n");
                break;
            }
            add_future_work(work);
            lock();
        }
        unlock();
        sleep(1);
        ++round;
    }

    close_files();
    close(server_fd);

    return 0;
}
