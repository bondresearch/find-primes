#include <gtest/gtest.h>

#include "../../common/tcp.h"
#include "../../common/work.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>

int silent = 1;
FILE *get_log_file()
{
    return NULL;
}

pid_t start_server()
{
    pid_t pid = fork();
    char * const argv[] = { "../server", "2", "silent", NULL };
    if (pid == 0) {
        execv(argv[0], argv);
        printf("child terminated\n");
        exit(0);
    }
    usleep(100000);
    return pid;
}

int stop_server(pid_t pid)
{
    usleep(100000);
    return kill(pid, SIGINT);
}

int is_in_file(int work, char *file)
{
    FILE *f = fopen(file, "r");
    char line[1024];
    char *ret;
    if (f == NULL)
        return 0;
    while (1) {
        ret = fgets(line, sizeof(line), f);
        if (ret == NULL) {
            fclose(f);
            return 0;
        }
        if (work == atoi(line)) {
            fclose(f);
            return 1;
        }
    }
    fclose(f);
    return 0;
}

int is_not_prime(int work)
{
    return is_in_file(work, "nonprimes.txt");
}

int is_prime(int work)
{
    return is_in_file(work, "primes.txt");
}

TEST(Server, StartStopServer) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    EXPECT_EQ(0, stop_server(pid));
}

TEST(Server, RequestWork) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    int fd = connect_to_tcp_server("127.0.0.1", 8080);
    EXPECT_TRUE(fd > 0);
    int cmd = REQUEST_WORK;
    EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
    int work;
    EXPECT_EQ(sizeof(work), recv(fd, &work, sizeof(work), 0));
    EXPECT_EQ(5, work);
    if (fd > 0)
        EXPECT_EQ(0, close(fd));
    EXPECT_EQ(0, stop_server(pid));
}

TEST(Server, Request100Work) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    for (int i = 0; i < 100; ++i) {
        int fd = connect_to_tcp_server("127.0.0.1", 8080);
        EXPECT_TRUE(fd > 0);
        int cmd = REQUEST_WORK;
        EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
        int work;
        EXPECT_EQ(sizeof(work), recv(fd, &work, sizeof(work), 0));
        if (fd > 0)
            EXPECT_EQ(0, close(fd));
    }
    EXPECT_EQ(0, stop_server(pid));
}

TEST(Server, RequestWorkReportWork) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    int fd = connect_to_tcp_server("127.0.0.1", 8080);
    EXPECT_TRUE(fd > 0);
    int cmd = REQUEST_WORK;
    EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
    int work;
    EXPECT_EQ(sizeof(work), recv(fd, &work, sizeof(work), 0));
    EXPECT_EQ(5, work);
    if (fd > 0)
        EXPECT_EQ(0, close(fd));
    fd = connect_to_tcp_server("127.0.0.1", 8080);
    EXPECT_TRUE(fd > 0);
    cmd = (work<<2) | WORK_DONE_NOT_PRIME;
    EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
    if (fd > 0)
        EXPECT_EQ(0, close(fd));
    usleep(100000);
    EXPECT_EQ(0, is_prime(work));
    EXPECT_EQ(1, is_not_prime(work));
    EXPECT_EQ(0, stop_server(pid));
}

TEST(Server, Request100WorkReportWork) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    int works[100];
    for (int i = 0; i < 100; ++ i) {
        int fd = connect_to_tcp_server("127.0.0.1", 8080);
        EXPECT_TRUE(fd > 0);
        int cmd = REQUEST_WORK;
        EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
        int work;
        EXPECT_EQ(sizeof(work), recv(fd, &work, sizeof(work), 0));
        works[i] = work;
        if (fd > 0)
            EXPECT_EQ(0, close(fd));
        fd = connect_to_tcp_server("127.0.0.1", 8080);
        EXPECT_TRUE(fd > 0);
        cmd = (work<<2) | (work % 2 == 0 ? WORK_DONE_PRIME : WORK_DONE_NOT_PRIME);
        EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
        if (fd > 0)
            EXPECT_EQ(0, close(fd));
    }
    usleep(100000);
    for (int i = 0; i < 100; ++i) {
        int work = works[i];
        EXPECT_EQ(work % 2 == 0 ? 1 : 0, is_prime(work));
        EXPECT_EQ(work % 2 == 0 ? 0 : 1, is_not_prime(work));
    }
    EXPECT_EQ(0, stop_server(pid));
}

TEST(Server, ReportWork) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    int work = 434;
    int fd = connect_to_tcp_server("127.0.0.1", 8080);
    EXPECT_TRUE(fd > 0);
    int cmd = (work<<2) | WORK_DONE_PRIME;
    EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
    if (fd > 0)
        EXPECT_EQ(0, close(fd));
    usleep(100000);
    EXPECT_EQ(1, is_prime(work));
    EXPECT_EQ(0, is_not_prime(work));
    EXPECT_EQ(0, stop_server(pid));
}

TEST(Server, ReportWorkExpectSkip) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    int work = 440;
    int fd = connect_to_tcp_server("127.0.0.1", 8080);
    EXPECT_TRUE(fd > 0);
    int cmd = (work<<2) | WORK_DONE_PRIME;
    EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
    if (fd > 0)
        EXPECT_EQ(0, close(fd));
    usleep(100000);
    EXPECT_EQ(1, is_prime(work));
    EXPECT_EQ(0, is_not_prime(work));
    for (int i = 0; i < 10; ++i) {
        int fd = connect_to_tcp_server("127.0.0.1", 8080);
        EXPECT_TRUE(fd > 0);
        int cmd = REQUEST_WORK;
        EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
        int work;
        EXPECT_EQ(sizeof(work), recv(fd, &work, sizeof(work), 0));
        EXPECT_NE(work, 440);
        if (fd > 0)
            EXPECT_EQ(0, close(fd));
    }
    
    EXPECT_EQ(0, stop_server(pid));
}

TEST(Server, ReportWorkExpectSkip2) {
    pid_t pid = start_server();
    EXPECT_TRUE(pid > 0);
    int work = 769;
    int fd = connect_to_tcp_server("127.0.0.1", 8080);
    EXPECT_TRUE(fd > 0);
    int cmd = (work<<2) | WORK_DONE_PRIME;
    EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
    if (fd > 0)
        EXPECT_EQ(0, close(fd));
    usleep(100000);
    EXPECT_EQ(1, is_prime(work));
    EXPECT_EQ(0, is_not_prime(work));
    int work2 = 0;
    for (int i = 0; work2 <= work; ++i) {
        int fd = connect_to_tcp_server("127.0.0.1", 8080);
        EXPECT_TRUE(fd > 0);
        int cmd = REQUEST_WORK;
        EXPECT_EQ(sizeof(cmd), send(fd, &cmd, sizeof(cmd), 0));
        int ret = recv(fd, &work2, sizeof(work2), 0);
        if (ret < sizeof(work2)) {
            close(fd);
            usleep(1100000);
            continue;
        }
        EXPECT_NE(work, work2);
        if (fd > 0)
            EXPECT_EQ(0, close(fd));
    }
    
    EXPECT_EQ(0, stop_server(pid));
}

int main(int argc, char **argv)
{
    unlink("primes.txt");
    unlink("nonprimes.txt");
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
