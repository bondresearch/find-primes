#pragma once

#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#define REQUEST_WORK 1
#define WORK_DONE_PRIME 2
#define WORK_DONE_NOT_PRIME 3

#ifndef DISABLE_LOGGING 
extern FILE *get_log_file();
extern int silent;

#define LOG(format, ...) \
    do { \
        if (silent) \
            break; \
        char buf[1024]; \
        struct tm tm; \
        struct timeval tv; \
        gettimeofday(&tv, NULL); \
        gmtime_r(&tv.tv_sec, &tm); \
        snprintf(buf, sizeof(buf), "%.2u:%.2u:%.2u:%.3ld - " format, tm.tm_hour, tm.tm_min, tm.tm_sec, tv.tv_usec / 1000, ##__VA_ARGS__); \
        printf("%s", buf); \
        if (get_log_file() != NULL) { \
            fprintf(get_log_file(), "%s", buf); \
            fflush(get_log_file()); \
        } \
    } while (0)
#else
#define LOG(format, ...)
#endif

