#include "tcp.h"
#include "work.h"

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>

#define MAX_EVENTS 1024

#ifndef STANDALONE
#include <sys/epoll.h>
int make_fd_non_blocking(int i_fd)
{
    int flags = fcntl(i_fd, F_GETFL, 0);
    if (flags < 0) {
        LOG("failed to get socket flags: %d\n", errno);
        return -1;
    }
    flags |= O_NONBLOCK;
    flags = fcntl(i_fd, F_SETFL, flags);
    if (flags < 0) {
        LOG("failed to set socket flags: %d\n", errno);
        return -1;
    }

    return 0;
}

int start_tcp_server(uint16_t port, int *epoll_fd)
{
    int server_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (server_fd < 0) {
        LOG("failed to create server socket: %d\n", errno);
        return -1;
    }
    int reuse = 1;
    int ret = setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)); 
    if (ret < 0) {
        LOG("failed to set REUSEADDR: %d\n", errno);
        return -1;
    }

    struct sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);
    ret = bind(server_fd, (struct sockaddr *)&address, sizeof(address));
    if (ret < 0) {
        LOG("failed to bind server socket to port %u: %d\n", port, errno);
        return -1;
    }

    ret = make_fd_non_blocking(server_fd);
    if (ret < 0) {
        close(server_fd);
        return -1;
    }

    ret = listen(server_fd, SOMAXCONN);
    if (ret < 0) {
        LOG("failed to start listening on server socket: %d\n", errno);
        close(server_fd);
        return -1;
    }
    *epoll_fd = epoll_create1(0);
    if (*epoll_fd < 0) {
        close(server_fd);
        LOG("failed to create epoll fd: %d\n", errno);
        return -1;
    }

    struct epoll_event event;
    event.data.fd = server_fd;
    event.events = EPOLLIN | EPOLLET;
    ret = epoll_ctl(*epoll_fd, EPOLL_CTL_ADD, server_fd, &event);
    if (ret < 0) {
        LOG("failed to add server epoll event: %d\n", errno);
        close(server_fd);
        return -1;
    }
    return server_fd;
}

int accept_new_connections(int server_fd, int epoll_fd)
{
    int ret;
    struct epoll_event event;
    while (1) {
        struct sockaddr_in in_addr;
        socklen_t in_len;
        int infd;

        in_len = sizeof in_addr;
        infd = accept (server_fd, (struct sockaddr *)&in_addr, &in_len);
        if (infd == -1) {
            if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                break;
            } else {
                perror ("accept");
                break;
            }
        }

        ret = make_fd_non_blocking (infd);
        if (ret == -1)
            return -1;

        event.data.fd = infd;
        event.events = EPOLLIN | EPOLLOUT | EPOLLET;
        ret = epoll_ctl (epoll_fd, EPOLL_CTL_ADD, infd, &event);
        if (ret == -1) {
            perror ("epoll_ctl");
            return -1;
        }
        char str[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &in_addr.sin_addr, str, INET_ADDRSTRLEN);
        LOG("accepted connection fd %d from %s:%u\n", infd, str, ntohs(in_addr.sin_port));
    }
    return 0;
}
#endif
int connect_to_tcp_server(const char *server, uint16_t port)
{
    struct hostent *he;
    int fd;
    he = gethostbyname(server);
    if (he == NULL) 
    {
        LOG("Failed to resolve hostname %s\n", server);
        return -1;
    }

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        LOG("failed to create socket\n");
        return -1;
    }
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    memcpy(&addr.sin_addr, he->h_addr, sizeof(addr.sin_addr));
    int ret = connect(fd, (struct sockaddr *)&addr, sizeof(addr));
    if (ret < 0) {
        LOG("failed to connect to server\n");
        return -1;
    }

    return fd;
}
