#pragma once

#include <stdint.h>

#ifndef STANDALONE
int make_fd_non_blocking(int i_fd);
int start_tcp_server(uint16_t port, int *epoll_fd);
int accept_new_connections(int server_fd, int epoll_fd);
#endif
int connect_to_tcp_server(const char *server, uint16_t port);
