@echo off
todos -d -q config.*
todos -d -q *.in
todos -d -q *.cpp
todos -d -q *.c
todos -d -q *.h
todos -d -q *.m4
todos -d -q *.
todos -d -q *.txt
todos -d -q *.asm
todos -d -q *.html
todos -d -q *.dox
todos -d -q *.css
todos -d -q *.inl
todos -d -q copying.lib
todos -d -q *.cxx
todos -d -q *.g++
todos -d -q *.gcc
todos -d -q *.patch

todos -d -q *.old
todos -d -q *.deps
todos -d -q *.cp7
todos -d -q primegen.res
todos -d -q *.tst?
todos -d -q *.test
todos -d -q *.pl
todos -d -q *.sh
todos -d -q *.pfgw
